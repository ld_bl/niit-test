// ��������� ������������� ����������� ���� � ����� ��������.
#define _CRT_SECURE_NO_WARNINGS
#include <string.h>
#include <stdio.h>
#define N 12
#define COEF_BEFORE_EDGE 110 // ����������� ��� ����� ������ "EDGE" ���
#define COEF_AFTER_EDGE 100 // ����������� ��� ����� ������ "EDGE" ���
#define EDGE 40 // ����������� �������
#define MIN_WEIGHT 30
#define MAX_WEIGHT 150
#define MIN_AGE 16 // ����������� ������� ��������
#define MAX_AGE 100 // ������������ ������� ��������
#define MIN_GROWTH 120 // ������������ ����
#define MAX_GROWTH 250 // ����������� ����

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	char name[N];
	int age = 0;
	int growth = 0;
	int result = 0;
	int sex = 0;
	int r_weight = 0;

	puts("Hello! What is your name?");
	fgets(name, N, stdin);
	if (name[strlen(name) - 1] == '\n')
		name[strlen(name) - 1] = 0;
	while (1)
	{
		printf("What is you age, %s?(full years)\n", name);
		scanf("%d", &age);
		if (age >= MIN_AGE && age <= MAX_AGE)
		{
			while (1)
			{
				printf("What is your growth, %s?(in centimeters)\n", name);
				scanf("%d", &growth);
				if (growth >= MIN_GROWTH && growth <= MAX_GROWTH)
				{
					while(1)
					{
						printf("What is your weight, %s?\n", name);
						scanf("%d", &r_weight);
						if (r_weight >= MIN_WEIGHT && r_weight <= MAX_WEIGHT)
						{
							while (1)
							{
								printf("What is your sex, %s?\n1 - Man, 2 - Woman\n", name);
								scanf("%d", &sex);
								if (sex >= 1 && sex <= 2)
								{
									if (age <= EDGE)
									{
										result = (growth - COEF_BEFORE_EDGE);
										if (result == r_weight)
											printf("Your weight is fine, %s!\n", name);
										else
											if (result < r_weight)
												printf("Oh, %s! Your are fat! Get on a diet!\n", name);
											else
												printf("Dear %s! You are malnourished! Eat something!\n", name);
									}
									else
									{
										result = (growth - COEF_AFTER_EDGE);
										if (result == r_weight)
											printf("Your weight is fine, %s!\n", name);
										else
											if (result < r_weight)
												printf("Oh, %s! Your are fat! Get on a diet!\n", name);
											else
												printf("Dear %s! You are malnourished! Eat something!\n", name);
									}
									break;
								}
								else
									puts("Input error!");
								clean_stdin();
							}
							break;
						}
						else
							puts("Input error!");
						clean_stdin();
					}
					break;
				}
				else
					puts("Input error!");
				clean_stdin();
			}
			break;
		}
		else
			puts("Input error!");
		clean_stdin();
	}
	return 0;
}